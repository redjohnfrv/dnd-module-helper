export type LinkId = string;

export type Overview = {
  block: string[];
};

export type Quest = {
  linkId: LinkId;
  title: string;
  overview: Overview;
  linkIds: LinkId[];
};

export type Person = {
  linkId: LinkId;
  name: string;
  overview: Overview;
  linkIds: LinkId[];
};
