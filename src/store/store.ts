import { createStore, GlobalState } from 'little-state-machine';
import { LinkId, Quest } from './types';
import { toast } from 'react-toastify';

createStore(
  {
    quests: [],
    persons: [],
  },
  { storageType: window.localStorage },
);

export function addQuest(state: GlobalState, payload: Quest) {
  const isLinkIdAlreadyExist = !!state.quests.filter(
    q => q.linkId === payload.linkId,
  );

  if (isLinkIdAlreadyExist) {
    toast('Already exist!', {
      type: 'error',
    });

    return state;
  }

  return {
    ...state,
    quests: [...state.quests, payload],
  };
}

export function deleteQuest(state: GlobalState, payload: LinkId) {
  const newQuests = state.quests.filter(q => q.linkId !== payload);

  return {
    ...state,
    quests: newQuests,
  };
}

export function updateQuest(state: GlobalState, payload: Quest) {
  const anotherQuests = state.quests.filter(q => q.linkId !== payload.linkId);

  return {
    ...state,
    quests: anotherQuests.concat(payload),
  };
}
