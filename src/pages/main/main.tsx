import css from './main.module.css';

const links = [
  {
    title: 'История',
    link: 'story',
  },
  {
    title: 'Квесты',
    link: 'quests',
  },
  {
    title: 'Персонажи',
    link: 'persons',
  },
];

export const Main = () => {
  return <div className={css.root}>main</div>;
};
