/// <reference types="react-scripts" />
import 'little-state-machine';
import { Person, Quest } from './store/types';

declare module 'little-state-machine' {
  interface GlobalState {
    quests: Quest[];
    persons: Person[];
  }
}

declare module '*.jpg';
declare module '*.png';
declare module '*.svg';
