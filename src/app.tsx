import React from 'react';
import { StateMachineProvider } from 'little-state-machine';
import { NavigatorProvider } from './router/router';
import { ToastRootContainer } from './tools/toast/toast-container';

function App() {
  return (
    <StateMachineProvider>
      <ToastRootContainer />
      <NavigatorProvider />
    </StateMachineProvider>
  );
}

export default App;
