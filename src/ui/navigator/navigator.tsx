import css from './navigator.module.css';
import { DefaultLink } from '../links';
import { PropsWithChildren } from 'react';
import { useLocation } from 'wouter';
import { Routes } from '../../router/router';
import { useStateMachine } from 'little-state-machine';

const links = [
  {
    title: 'История',
    link: Routes.STORY,
  },
  {
    title: 'Квесты',
    link: Routes.QUESTS,
  },
  {
    title: 'Персонажи',
    link: Routes.PERSONS,
  },
];

export const Navigator = ({ children }: PropsWithChildren) => {
  const path = useLocation();
  const { state } = useStateMachine();

  return (
    <>
      <div className={css.links}>
        {links.map(link => (
          <DefaultLink
            key={link.link}
            link={link}
            isActive={path[0] === link.link}
          />
        ))}
      </div>

      {children}
    </>
  );
};
