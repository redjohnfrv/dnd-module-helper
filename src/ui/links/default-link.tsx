import css from './link.module.css';
import { Link as NavigationLink } from 'wouter';
import clsx from 'clsx';
import { LinkId } from '../../store/types';

export type Link = {
  link: LinkId;
  title?: string;
};

export interface DefaultLinkProps {
  link: Link;
  className?: string;
  isActive: boolean;
}

export const DefaultLink = ({
  link,
  className,
  isActive,
}: DefaultLinkProps) => {
  return (
    <NavigationLink
      className={clsx([css.link, isActive && css.activeLink, className])}
      href={link.link}>
      {link.title}
    </NavigationLink>
  );
};
