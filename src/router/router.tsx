import { MainPage, StoryPage } from '../pages';
import { Navigator } from '../ui/navigator';
import React from 'react';
import { Route, Switch } from 'wouter';

export enum Routes {
  MAIN = '/',
  STORY = '/story',
  QUESTS = '/quests',
  PERSONS = '/persons',
}

const Page = ({ page }: { page: JSX.Element }) => {
  return <Navigator>{page}</Navigator>;
};

export const NavigatorProvider = () => {
  return (
    <>
      <Switch>
        <Route
          path={Routes.MAIN}
          component={() => <Page page={<MainPage />} />}
        />
        <Route
          path={Routes.STORY}
          component={() => <Page page={<StoryPage />} />}
        />
        <Route
          path={Routes.QUESTS}
          component={() => <Page page={<div>Квесты</div>} />}
        />
        <Route
          path={`${Routes.QUESTS}/:id`}
          component={() => <Page page={<div>Квесты</div>} />}
        />
        <Route
          path={Routes.PERSONS}
          component={() => <Page page={<div>Персонажи</div>} />}
        />
        <Route
          path={`${Routes.PERSONS}/:id`}
          component={() => <Page page={<div>Персонажи id</div>} />}
        />
      </Switch>
    </>
  );
};
